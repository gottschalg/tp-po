from pulp import *
import matplotlib.pyplot as plt
from matplotlib.path import Path
from matplotlib.patches import PathPatch
import numpy as np

line1 = lambda x0, T: (-6*x0+T)/10.5
line2 = lambda x0, P: (-4*x0+P)/2
line3 = lambda x0, D: (D)
o  = lambda x0, Z: (-3.6*x0 + Z)/5.4



############################################
# Class for plot graphs
############################################
class GraphPngPlot():
    def drawLine(self, lstX, lstY, tslabel):
        line = plt.plot(lstX,lstY,label=tslabel)
        plt.legend()
        return line

    def drawGraphs(self, type, s):
        plt.title(type['desc'], fontsize=9)
        plt.axis((0,9000,0,10000))
        plt.annotate('Z (' + str(s['x0']) + ',' + str(s['x1']) + ')', xy=(s['x0'], s['x1']), xytext=(s['x0'], s['x1']), arrowprops=dict(facecolor='black',shrink=0.05))
        dss = DecisionSuporteSystem()
        pe = dss.getFactivePoints(type['T'],type['P'],type['D'])
        for p in pe :
           plt.annotate(p['name'] + '(' + str(int(p['x'])) + ',' + str(int(p['y'])) + ')', xy=(p['x'], p['y']), xytext=(p['x'], p['y']), arrowprops=dict(facecolor='black',shrink=0.05))
        plt.savefig(type['name'] + '.png', format='png')

############################################
# Class decision suporte system
############################################
class DecisionSuporteSystem():
    def getFactivePoints(self, T, P, D) :
         p1 = {'name':'p1', 'x':0, 'y':D}
         x = ((10.5*D)-T)/-6
         y = line1 (x,T)
         p2 = {'name':'p2', 'x':int(x),'y':int(y)}
         p4 = {'name':'p4', 'x':5000, 'y':0}
         return [p1,p2,p4]

    def execute(self, type):
        T  = type['T']
        P  = type['P']
        D  = type['D']
        result = LpProblem("TP1", LpMaximize)
        x = [LpVariable("x"+str(i), 0) for i in range(2)]
        result += 3.6*x[0] + 5.4*x[1]
        result += 6*x[0] + 10.5*x[1] <= T
        result += 4*x[0] + 2*x[1] <= P
        result +=  x[1] <= D
        result +=  x[0] >= 0
        result +=  x[1] >= 0
        status = result.solve()
        print("Status:"+ LpStatus[status])
        s={}
        for v in result.variables():
            s[v.name] = int(v.varValue)
            print(v.name, "=", v.varValue)
        s['z'] = int(value(result.objective))
        print("objective=", value(result.objective))
        return s

    def plotGraph(self, T,P,D,C,AT):
        texto = '(T=' + str(T) + ' P=' + str(P) + ' D=' + str(D) + ')'
        p1={'name': 'type1', 'desc':'Parametros normais ' + texto,'T':T,'P':P,'D':D}

        texto = '(T=' + str(T) + ' P=' + str(P) + ' D=' + str(D*(1.+C/100.0)) + ')'
        p2={'name': 'type2', 'desc':'D aumentado em ' + str(C)  + '%' + texto,'T':T,'P':P,'D':D*(1.+C/100.0)}

        texto = '(T=' + str(T*(1.0+AT/100.0)) + ' P=' + str(P) + ' D=' + str(D) + ')'
        p3={'name': 'type3', 'desc':'T aumentado em ' + str(AT) + '%' + texto,'T':T*(1.0+AT/100.0),'P':P,'D':D}

        texto = '(T=' + str(T*(1.0+AT/100.0)) + ' P=' + str(P) + ' D=' + str(D*(1.+C/100.0)) + ')'
        p4={'name': 'type4', 'desc':'D e T aumentados em ' + str(C) + '% e ' + str(AT) + '% respectivamente ' + texto,'T':p3['T'],'P':P,'D':p2['D']}

        listX  = np.linspace(0, 10000)
        for type in [p1,p2,p3,p4]:
           print (type['name'],type['T'],type['P'],type['D'])
           s = self.execute(type)
           listY1 = np.array(line1(listX,type['T']))
           listY2 = np.array(line2(listX,type['P']))
           listY3 = np.array([type['D']]*len(listY2))
           listY4 = np.array(o(listX,s['z']))
           plt.clf()

           graph = GraphPngPlot()

           graph.drawLine(listX,listY1,'line1 (6*x1+10.5*x2<=' + str(int(type['T'])) + ')')
           graph.drawLine(listX,listY2,'line2 (4*x1+2*x2   <=' + str(int(type['P'])) + ')')
           graph.drawLine(listX,listY3,'line3 (x2          <=' + str(int(type['D'])) + ')')
           graph.drawLine(listX,listY4,'Objet.->z=3.6*x1+5.4*x2')
           plt.fill_between(listX,0,listY1,alpha=0.1)
           plt.fill_between(listX,0,listY2,alpha=0.1)
           plt.fill_between(listX,0,listY3,alpha=0.1)
           graph.drawGraphs(type,s)

def main():
   T  = 48000
   P  = 20000
   D  = 3500
   C  = 20
   AT = 25
   dss = DecisionSuporteSystem()
   dss.plotGraph( T, P, D, C, AT)


main()
